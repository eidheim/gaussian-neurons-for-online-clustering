# Gaussian Neurons for Online Clustering

This repository contains the source files used to produce the figures in the article:

Eidheim, O. C. (2022). Revisiting Gaussian Neurons for Online Clustering with Unknown Number of Clusters.

arXiv link: https://arxiv.org/abs/2205.00920

The source files in this repository were solely made to produce the figures and experimental results in the above article, and were therefore not created with optimization nor extendibility in mind. However, the bachelor thesis [Parallelization of Local Learning Rules](https://ntnuopen.ntnu.no/ntnu-xmlui/handle/11250/3009254) presents parallelized implementations of the learning rule, and their source code can be found at https://github.com/ikhovind/Parallelization-of-Local-Learning-Rules. 

## Dependencies

- Python 3
- NumPy
- Matplotlib
- TorchVision
- Scikit-learn
- LaTeX

## Run experiments and save PGF figures

<p><code>$ <b>python3 figure</b>1|2abcd|2e|2fg|2h|3|4|5abcde|5f<b>.py</b> # For example: python3 figure2abcd.py</code></p>

## Use previously run experiments and save PGF figures

1. Comment out the `experiment()` calls
2. <p><code>$ <b>python3 figure</b>1|2abcd|2e|2fg|2h|3|4|5abcde|5f<b>.py</b> # For example: python3 figure2abcd.py</code></p>

## Use previously run experiments and show figures

1. Comment out the `experiment()` calls
2. Comment out the `matplotlib.use("pgf")` call
3. Comment out the `plt.savefig()` calls
4. Uncomment the `plt.show()` calls
5. <p><code>$ <b>python3 figure</b>1|2abcd|2e|2fg|2h|3|4|5abcde|5f<b>.py</b> # For example: python3 figure2abcd.py</code></p>
