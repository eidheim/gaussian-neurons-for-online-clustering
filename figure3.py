import torchvision
import matplotlib

matplotlib.use("pgf")
import matplotlib.pyplot as plt
import math
import numpy as np

figsize_scale = 0.2

matplotlib.rcParams.update({
    "figure.figsize": np.array([9. * 1.5, 4. * 1.5]) * figsize_scale,
    "figure.subplot.wspace": 0.1,
    "figure.subplot.hspace": 0.1,
    "font.family": "serif",
    "font.size": 9,
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
data = mnist_train.data.double()
data = data / data.max()
data = data.numpy()


def get_batch(batch_size, filter_size):
    filter_radius = int((filter_size - 1) / 2)
    batch_indexes = np.floor(np.random.rand(batch_size, 1) * data.shape[0]).astype(int)
    batch_indexes = np.concatenate((batch_indexes, np.floor(filter_radius + np.random.rand(batch_size, 1) * (data.shape[1] - filter_radius * 2)).astype(int)),
                                   axis=1)
    batch_indexes = np.concatenate((batch_indexes, np.floor(filter_radius + np.random.rand(batch_size, 1) * (data.shape[2] - filter_radius * 2)).astype(int)),
                                   axis=1)

    batch = np.zeros((batch_size, filter_size, filter_size))
    for count, index in enumerate(batch_indexes):
        batch[count] = data[index[0], index[1] - filter_radius:index[1] + filter_radius + 1, index[2] - filter_radius:index[2] + filter_radius + 1]

    return batch


learning_rate = .1


class Model:

    def __init__(self, sigma, lambda_, filter_size, filters):
        self.sigma = sigma
        self.lambda_ = lambda_
        self.mu = np.random.rand(filters, filter_size, filter_size)

    def f(self, i, x):
        return np.exp(-np.square(x - self.mu[i]).sum() / self.sigma)

    def update(self, x):
        diff = np.zeros(self.mu.shape)
        for i1 in range(0, self.mu.shape[0]):
            diff[i1] += self.f(i1, x) * (x - self.mu[i1])

            for i2 in range(0, self.mu.shape[0]):
                if (i1 != i2):
                    diff[i1] -= 2. * self.lambda_ * (self.mu[i2] - self.mu[i1]) * self.f(i1, self.mu[i2])

        self.mu += learning_rate * diff / self.sigma


def experiment(subfigure, sigma, lambda_, filter_size, batches, filters=4 * 4):
    np.random.seed(0)
    model = Model(sigma=sigma, lambda_=lambda_, filter_size=filter_size, filters=filters)
    batch_size = 1000

    for i in range(0, batches):
        for x in get_batch(batch_size, filter_size):
            model.update(x)
        print(f"Completed batch {i+1}")

    np.save(f"figure3{subfigure}.npy", model.mu)


experiment(subfigure="a", sigma=4.14, lambda_=.015, filter_size=9, batches=10000, filters=40)
experiment(subfigure="b", sigma=9.91, lambda_=.01, filter_size=13, batches=10000, filters=40)


def figure(images):
    fig, axes = plt.subplots(nrows=4, ncols=10)
    for i, ax in enumerate(axes.flat):
        im = ax.imshow(images[i], vmin=0, vmax=1)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    return fig, axes


def save_all():
    for subfigure in ["a", "b"]:
        mu = np.load(f"figure3{subfigure}.npy")
        fig, _ = figure(mu)
        plt.tight_layout(pad=0.0)
        # plt.show()
        plt.savefig(f"figure3{subfigure}.pgf", bbox_inches='tight', pad_inches=.0)

    # Colorbar
    fig, ax = plt.subplots(figsize=np.array([2.0, 4. * 1.5]) * figsize_scale)
    norm = matplotlib.colors.Normalize(vmin=0, vmax=1)
    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm), cax=ax, pad=0.0)
    plt.tight_layout(pad=0.0)
    # plt.show()
    plt.savefig("figure3_colorbar.pgf", bbox_inches='tight', pad_inches=.0)


save_all()
