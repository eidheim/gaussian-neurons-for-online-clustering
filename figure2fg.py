import torchvision
import matplotlib

matplotlib.use("pgf")
import matplotlib.pyplot as plt
import math
import numpy as np

figsize_scale = 0.2

matplotlib.rcParams.update({
    "figure.figsize": np.array([6.4, 6.4]) * figsize_scale,
    "figure.subplot.wspace": 0.1,
    "figure.subplot.hspace": 0.1,
    "font.family": "serif",
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
data = mnist_train.data.double()
data = data / data.max()
data = data.numpy()

number_indexes = [[] for i in range(10)]
for i, target in enumerate(mnist_train.targets):
    number_indexes[target].append(i)


def get_batch(batch_size, number):
    batch_indexes = np.floor(np.random.rand(batch_size, 1) * len(number_indexes[number])).astype(int)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[1] - 4)).astype(int)), axis=1)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[2] - 4)).astype(int)), axis=1)

    batch = np.zeros((batch_size, 5, 5))
    for count, index in enumerate(batch_indexes):
        batch[count] = data[number_indexes[number][index[0]], index[1] - 2:index[1] + 3, index[2] - 2:index[2] + 3]

    return batch


learning_rate = .1


class Model:

    def __init__(self, sigma, lambda_):
        self.sigma = sigma
        self.lambda_ = lambda_
        self.mu = np.random.rand(4 * 4, 5, 5)

    def f(self, i, x):
        return np.exp(-np.square(x - self.mu[i]).sum() / self.sigma)

    def update(self, x):
        diff = np.zeros(self.mu.shape)
        for i1 in range(0, self.mu.shape[0]):
            diff[i1] += self.f(i1, x) * (x - self.mu[i1])

            for i2 in range(0, self.mu.shape[0]):
                if (i1 != i2):
                    diff[i1] -= 2. * self.lambda_ * (self.mu[i2] - self.mu[i1]) * self.f(i1, self.mu[i2])

        self.mu += learning_rate * diff / self.sigma


np.random.seed(0)
model = Model(sigma=.5, lambda_=.5)


def experiment(subfigure, batches):
    batch_size = 1000

    for i in range(0, batches):
        for x in get_batch(batch_size, 1 if subfigure == "f" else 2):
            model.update(x)
        print(f"Completed batch {i+1}")

    np.save(f"figure2{subfigure}.npy", model.mu)


experiment(subfigure="f", batches=1000)
experiment(subfigure="g", batches=1000)


def figure(images):
    fig, axes = plt.subplots(nrows=int(math.sqrt(images.shape[0])), ncols=int(math.sqrt(images.shape[0])))
    for i, ax in enumerate(axes.flat):
        im = ax.imshow(images[i], vmin=0, vmax=1)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    return fig, axes


def save_all():
    for subfigure in ["f", "g"]:
        mu = np.load(f"figure2{subfigure}.npy")
        fig, _ = figure(mu)
        plt.tight_layout(pad=0.0)
        # plt.show()
        plt.savefig(f"figure2{subfigure}.pgf", bbox_inches='tight', pad_inches=.0)


save_all()
