import matplotlib

matplotlib.use("pgf")
import matplotlib.pyplot as plt
import numpy as np

matplotlib.rcParams.update({
    "figure.figsize": np.array(matplotlib.rcParams["figure.figsize"]) * .6,
    "font.family": "serif",
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

resolution = 100

mu11, mu12 = np.meshgrid(np.linspace(.0, 1.0, resolution), np.linspace(.0, 1.0, resolution))

x = np.array([.3, .7])
mu = np.array([[.0, .0], [.7, .3]])
sigma = np.array([.2, .2])
lambda_ = .5


def f(mu, sigma, x):
    return np.exp(-np.square(x - mu).sum() / sigma)


F = np.zeros((resolution, resolution))

for mu11_index in range(0, resolution):
    for mu12_index in range(0, resolution):

        mu_ = lambda i: mu[i] if i != 0 else np.array([mu11[mu11_index, mu12_index], mu12[mu11_index, mu12_index]])

        for i in range(0, mu.shape[0]):
            F[mu11_index, mu12_index] -= f(mu_(i), sigma[i], x)
            for j in range(0, mu.shape[0]):
                if j != i:
                    F[mu11_index, mu12_index] += lambda_ * f(mu_(j), sigma[j], mu_(i))

c = plt.contourf(mu11, mu12, F, levels=20)

plt.xlabel(r"$\mu_{1,1}$")
plt.ylabel(r"$\mu_{1,2}$")
plt.plot(.3, .7, '.', color="white")
plt.plot(.7, .3, '.', color="black")
plt.text(.3 + .02, .7 - .02, r"$\boldsymbol{x}$", color="white")
plt.text(.7 + .02, .3 - .02, r"$\boldsymbol{\mu}_2$", color="black")
plt.colorbar(c)

ind = np.unravel_index(np.argmin(F, axis=None), F.shape)
print(f"Minimum of F is at mu_1 = {[mu11[ind], mu12[ind]]} (increase resolution to get a better estimate)")

plt.tight_layout(pad=0.0)
plt.savefig("figure1.pgf", bbox_inches='tight', pad_inches=.0)
