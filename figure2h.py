import torchvision
import matplotlib

# matplotlib.use("pgf")
import matplotlib.pyplot as plt
import math
import numpy as np
from sklearn.cluster import KMeans

figsize_scale = 0.2

matplotlib.rcParams.update({
    "figure.figsize": np.array([6.4, 6.4]) * figsize_scale,
    "figure.subplot.wspace": 0.1,
    "figure.subplot.hspace": 0.1,
    "font.family": "serif",
    "font.size": 9,
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
data = mnist_train.data.double()
data = data / data.max()
data = data.numpy()


def get_batch(batch_size):
    batch_indexes = np.floor(np.random.rand(batch_size, 1) * data.shape[0]).astype(int)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[1] - 4)).astype(int)), axis=1)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[2] - 4)).astype(int)), axis=1)

    batch = np.zeros((batch_size, 5, 5))
    for count, index in enumerate(batch_indexes):
        batch[count] = data[index[0], index[1] - 2:index[1] + 3, index[2] - 2:index[2] + 3]

    return batch


def experiment():
    np.random.seed(0)

    batch_size = 1000 * 1000
    batch = get_batch(batch_size)

    kmeans = KMeans(n_clusters=16, random_state=0).fit(batch.reshape(batch_size, -1))

    np.save(f"figure2h.npy", kmeans.cluster_centers_.reshape(16, 5, 5))


experiment()


def figure(images):
    fig, axes = plt.subplots(nrows=int(math.sqrt(images.shape[0])), ncols=int(math.sqrt(images.shape[0])))
    for i, ax in enumerate(axes.flat):
        im = ax.imshow(images[i], vmin=0, vmax=1)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    return fig, axes


def save_all():
    mu = np.load(f"figure2h.npy")
    fig, _ = figure(mu)
    plt.tight_layout(pad=0.0)
    plt.show()
    # plt.savefig(f"figure2h.pgf", bbox_inches='tight', pad_inches=.0)


save_all()
