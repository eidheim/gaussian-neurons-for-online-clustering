import torchvision
import matplotlib

matplotlib.use("pgf")
import matplotlib.pyplot as plt
import math
import numpy as np

figsize_scale = 0.2 * 3. / 4.

matplotlib.rcParams.update({
    "figure.figsize": np.array([6.4, 6.4]) * figsize_scale,
    "figure.subplot.wspace": 0.1,
    "figure.subplot.hspace": 0.1,
    "font.family": "serif",
    "font.size": 9,
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
data = mnist_train.data.double()
data = data / data.max()
data = data.numpy()


def get_batch(batch_size, filter_size, update_widths):
    if not update_widths:
        filter_radius = int((filter_size - 1) / 2)
        batch_indexes = np.floor(np.random.rand(batch_size, 1) * data.shape[0]).astype(int)
        batch_indexes = np.concatenate(
            (batch_indexes, np.floor(filter_radius + np.random.rand(batch_size, 1) * (data.shape[1] - filter_radius * 2)).astype(int)), axis=1)
        batch_indexes = np.concatenate(
            (batch_indexes, np.floor(filter_radius + np.random.rand(batch_size, 1) * (data.shape[2] - filter_radius * 2)).astype(int)), axis=1)

        batch = []
        for index in batch_indexes:
            batch.append([data[index[0], index[1] - filter_radius:index[1] + filter_radius + 1, index[2] - filter_radius:index[2] + filter_radius + 1], 0.])

        return batch

    filter_radius = int((filter_size - 1) / 2)
    batch_indexes = np.floor(np.random.rand(batch_size, 1) * data.shape[0]).astype(int)
    batch_indexes = np.concatenate(
        (batch_indexes, np.floor(filter_radius + 1 + np.random.rand(batch_size, 1) * (data.shape[1] - (filter_radius + 1) * 2)).astype(int)), axis=1)
    batch_indexes = np.concatenate(
        (batch_indexes, np.floor(filter_radius + 1 + np.random.rand(batch_size, 1) * (data.shape[2] - (filter_radius + 1) * 2)).astype(int)), axis=1)

    batch = []
    for count, index in enumerate(batch_indexes):
        samples = np.zeros((9, filter_size, filter_size))
        i = 0
        for dy in range(-1, 2):
            for dx in range(-1, 2):
                samples[i] = data[index[0], index[1] + dy - filter_radius:index[1] + dy + filter_radius + 1,
                                  index[2] + dx - filter_radius:index[2] + dx + filter_radius + 1] + np.random.normal(0., 0.1, (filter_size, filter_size))
                i = i + 1
        mean = np.mean(samples, 0)
        variance = np.square(samples - mean).sum(axis=(1, 2)).mean()
        if variance == 0.:
            variance = 1e-10
        batch.append([mean, variance * 2.])

    return batch


learning_rate_mu = .1
learning_rate_sigma = .1


class Model:

    def __init__(self, sigma_init, lambda_, filter_size, filters, update_widths):
        self.sigma = np.ones((filters)) * sigma_init
        self.lambda_ = lambda_
        self.mu = np.random.rand(filters, filter_size, filter_size)
        self.update_widths = update_widths

    def f(self, mu, sigma, x):
        return np.exp(-np.square(x - mu).sum() / sigma)

    def update(self, x):
        diff_mu = np.zeros(self.mu.shape)
        for i1 in range(0, self.mu.shape[0]):
            diff_mu[i1] += self.f(self.mu[i1], self.sigma[i1], x[0]) * (x[0] - self.mu[i1]) / self.sigma[i1]

            for i2 in range(0, self.mu.shape[0]):
                if (i1 != i2):
                    diff_mu[i1] -= self.lambda_ * (self.f(self.mu[i1], self.sigma[i1], self.mu[i2]) / self.sigma[i1] +
                                                   self.f(self.mu[i2], self.sigma[i2], self.mu[i1]) / self.sigma[i2]) * (self.mu[i2] - self.mu[i1])

        self.mu += learning_rate_mu * diff_mu

        if self.update_widths:
            diff_sigma = np.zeros(self.sigma.shape)
            for i1 in range(0, self.sigma.shape[0]):
                fx = self.f(x[0], x[1], self.mu[i1])
                for i2 in range(0, self.mu.shape[0]):
                    if i2 != i1:
                        fx -= 2. * self.lambda_ * self.f(self.mu[i2], self.sigma[i2], self.mu[i1])
                    if fx <= 0.:
                        break
                if fx > 0.:
                    diff_sigma[i1] = fx * self.f(self.mu[i1], self.sigma[i1], x[0]) * (x[1] - self.sigma[i1])
            self.sigma += learning_rate_sigma * diff_sigma


def experiment(subfigure, sigma_init, lambda_, batches, filter_size, filters, update_widths=True):
    np.random.seed(0)
    model = Model(sigma_init=sigma_init, lambda_=lambda_, filter_size=filter_size, filters=filters, update_widths=update_widths)
    batch_size = 1000

    for i in range(0, batches):
        for x in get_batch(batch_size, filter_size=filter_size, update_widths=update_widths):
            model.update(x)
        print(f"Completed batch {i+1}")
        print(np.round(model.sigma, 1))

    np.save(f"figure5{subfigure}.npy", model.mu)


experiment("a", sigma_init=6., lambda_=.5, batches=10000, filter_size=5, filters=3 * 3, update_widths=False)
experiment("b", sigma_init=14., lambda_=.5, batches=10000, filter_size=5, filters=3 * 3, update_widths=False)
experiment("c", sigma_init=6., lambda_=.5, batches=10000, filter_size=5, filters=3 * 3, update_widths=True)
experiment("d", sigma_init=10., lambda_=.5, batches=10000, filter_size=5, filters=3 * 3, update_widths=True)
experiment("e", sigma_init=14., lambda_=.5, batches=10000, filter_size=5, filters=3 * 3, update_widths=True)


def figure(images):
    fig, axes = plt.subplots(nrows=int(math.sqrt(images.shape[0])), ncols=int(math.sqrt(images.shape[0])))
    for i, ax in enumerate(axes.flat):
        im = ax.imshow(images[i], vmin=0, vmax=1)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    return fig, axes


def save_all():
    for subfigure in ["a", "b", "c", "d", "e"]:
        mu = np.load(f"figure5{subfigure}.npy")
        fig, _ = figure(mu)
        plt.tight_layout(pad=0.0)
        # plt.show()
        plt.savefig(f"figure5{subfigure}.pgf", bbox_inches='tight', pad_inches=.0)

    # Colorbar
    fig, ax = plt.subplots(figsize=np.array([2.5, 6.4]) * figsize_scale)
    norm = matplotlib.colors.Normalize(vmin=0, vmax=1)
    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm), cax=ax, pad=0.0)
    plt.tight_layout(pad=0.0)
    # plt.show()
    plt.savefig("figure5_colorbar.pgf", bbox_inches='tight', pad_inches=.0)


save_all()
