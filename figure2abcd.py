import torchvision
import matplotlib

matplotlib.use("pgf")
import matplotlib.pyplot as plt
import math
import numpy as np

figsize_scale = 0.2

matplotlib.rcParams.update({
    "figure.figsize": np.array([6.4, 6.4]) * figsize_scale,
    "figure.subplot.wspace": 0.1,
    "figure.subplot.hspace": 0.1,
    "font.family": "serif",
    "font.size": 9,
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
data = mnist_train.data.double()
data = data / data.max()
data = data.numpy()


def get_batch(batch_size):
    batch_indexes = np.floor(np.random.rand(batch_size, 1) * data.shape[0]).astype(int)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[1] - 4)).astype(int)), axis=1)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[2] - 4)).astype(int)), axis=1)

    batch = np.zeros((batch_size, 5, 5))
    for count, index in enumerate(batch_indexes):
        batch[count] = data[index[0], index[1] - 2:index[1] + 3, index[2] - 2:index[2] + 3]

    return batch


learning_rate = .1


class Model:

    def __init__(self, sigma, lambda_):
        self.sigma = sigma
        self.lambda_ = lambda_
        self.mu = np.random.rand(4 * 4, 5, 5)

    def f(self, i, x):
        return np.exp(-np.square(x - self.mu[i]).sum() / self.sigma)

    def update(self, x):
        diff = np.zeros(self.mu.shape)
        for i1 in range(0, self.mu.shape[0]):
            diff[i1] += self.f(i1, x) * (x - self.mu[i1])

            for i2 in range(0, self.mu.shape[0]):
                if (i1 != i2):
                    diff[i1] -= 2. * self.lambda_ * (self.mu[i2] - self.mu[i1]) * self.f(i1, self.mu[i2])

        self.mu += learning_rate * diff / self.sigma


def experiment(subfigure, sigma, lambda_, batches):
    np.random.seed(0)
    model = Model(sigma=sigma, lambda_=lambda_)
    batch_size = 1000

    for i in range(0, batches):
        for x in get_batch(batch_size):
            model.update(x)
        print(f"Completed batch {i+1}")

    np.save(f"figure2{subfigure}.npy", model.mu)


experiment(subfigure="a", sigma=1., lambda_=.5, batches=1000)
experiment(subfigure="b", sigma=1., lambda_=.5, batches=10000)
experiment(subfigure="c", sigma=.5, lambda_=.5, batches=10000)
experiment(subfigure="d", sigma=1., lambda_=1. / 9., batches=10000)


def figure(images):
    fig, axes = plt.subplots(nrows=int(math.sqrt(images.shape[0])), ncols=int(math.sqrt(images.shape[0])))
    for i, ax in enumerate(axes.flat):
        im = ax.imshow(images[i], vmin=0, vmax=1)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    return fig, axes


def save_all():
    for subfigure in ["a", "b", "c", "d"]:
        mu = np.load(f"figure2{subfigure}.npy")
        fig, _ = figure(mu)
        plt.tight_layout(pad=0.0)
        # plt.show()
        plt.savefig(f"figure2{subfigure}.pgf", bbox_inches='tight', pad_inches=.0)

    # Colorbar
    fig, ax = plt.subplots(figsize=np.array([2.0, 6.4]) * figsize_scale)
    norm = matplotlib.colors.Normalize(vmin=0, vmax=1)
    fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm), cax=ax, pad=0.0)
    plt.tight_layout(pad=0.0)
    # plt.show()
    plt.savefig("figure2_colorbar.pgf", bbox_inches='tight', pad_inches=.0)


save_all()

# Euclidean distance from data domain center [0.5, ... 0.5] divided by data domain's max distance to domain center
# mu = np.load("figure2a.npy")
# for m in mu:
#     print(np.round(np.sqrt(np.square(m - np.ones((5, 5)) * .5).sum()) / np.sqrt(np.square(np.ones((5, 5)) - np.ones((5, 5)) * .5).sum()), 1))

# Cosine similarity between the final and initial filters
# mu = np.load("figure2a.npy")
# np.random.seed(0)
# initial_mu = Model(sigma=1., lambda_=.5).mu
# for i, m in enumerate(mu):
#     print(np.round((m * initial_mu[i]).sum() / (np.sqrt(np.square(m).sum()) * np.sqrt(np.square(initial_mu[i]).sum())), 1))
