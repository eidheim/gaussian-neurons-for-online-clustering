import torchvision
import matplotlib

matplotlib.use("pgf")
import matplotlib.pyplot as plt
import math
import numpy as np
import os.path

figsize_scale = 0.3

matplotlib.rcParams.update({
    "figure.figsize": np.array([10., 5.]) * figsize_scale,
    "figure.subplot.wspace": 0.1,
    "figure.subplot.hspace": 0.1,
    "font.family": "serif",
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.preamble": r"\usepackage{amsmath}"
})

if os.path.isfile("data/cifar-10.npy"):
    data = np.load("data/cifar-10.npy")
else:
    cifar10_train = torchvision.datasets.CIFAR10('./data', train=True, download=True)
    data = np.zeros((50000, 32, 32, 3))
    for i, image in enumerate(cifar10_train):
        data[i] = np.asarray(image[0])
    data = data / data.max()
    np.save("data/cifar-10.npy", data)


def get_batch(batch_size):
    batch_indexes = np.floor(np.random.rand(batch_size, 1) * data.shape[0]).astype(int)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[1] - 4)).astype(int)), axis=1)
    batch_indexes = np.concatenate((batch_indexes, np.floor(2 + np.random.rand(batch_size, 1) * (data.shape[2] - 4)).astype(int)), axis=1)

    batch = np.zeros((batch_size, 5, 5, 3))
    for count, index in enumerate(batch_indexes):
        batch[count] = data[index[0], index[1] - 2:index[1] + 3, index[2] - 2:index[2] + 3]

    return batch


learning_rate = .1


class Model:

    def __init__(self, sigma, lambda_, filter_count):
        self.sigma = sigma
        self.lambda_ = lambda_
        self.mu = np.random.rand(filter_count, 5, 5, 3)

    def f(self, i, x):
        return np.exp(-np.square(x - self.mu[i]).sum() / self.sigma)

    def update(self, x):
        diff = np.zeros(self.mu.shape)
        for i1 in range(0, self.mu.shape[0]):
            diff[i1] += self.f(i1, x) * (x - self.mu[i1])

            for i2 in range(0, self.mu.shape[0]):
                if (i1 != i2):
                    diff[i1] -= 2. * self.lambda_ * (self.mu[i2] - self.mu[i1]) * self.f(i1, self.mu[i2])

        self.mu += learning_rate * diff / self.sigma


def experiment(sigma, lambda_, batches, filter_count=4 * 4):
    np.random.seed(0)
    model = Model(sigma=sigma, lambda_=lambda_, filter_count=filter_count)
    batch_size = 1000

    for i in range(0, batches):
        for x in get_batch(batch_size):
            model.update(x)
        print(f"Completed batch {i+1}")

    np.save("figure4.npy", model.mu)


experiment(sigma=1.75, lambda_=.01, batches=1000, filter_count=50)


def figure(images):
    fig, axes = plt.subplots(nrows=5, ncols=10)
    for i, ax in enumerate(axes.flat):
        im = ax.imshow(images[i], vmin=0, vmax=1)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    return fig, axes


def save_all():
    mu = np.load(f"figure4.npy")
    fig, _ = figure(mu)
    plt.tight_layout(pad=0.0)
    # plt.show()
    plt.savefig(f"figure4.pgf", bbox_inches='tight', pad_inches=.0)


save_all()
